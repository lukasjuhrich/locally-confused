title: Solutions to Humphreys Chap. 3
---
abstract: As a challenge, I attempt to solve exercises in Humphreys' Lie algebra book with as little space consumption as possible. This will force me to make the core argument rather clear and on-the-point.
---
newbody:

`\DeclareMathOperator{\ad}{ad}` *(The assignments are omitted, because it's probably not a wise idea to copy them word-for-word on a public site)*

1. By jacobi, the commutator of two ideals is again an ideal.
2. `L/I` is abelian iff I contains `[LL]`, so `\Rightarrow` is clear. OTOH, `L_i/L_{i+1}` abelian implies `L_{i+1}\geq [L_i L_i]`, implying this squashes the derived series.
3. The generators `h,x,y` satisfy `[xy]=h` and `[hx]=2x=0=[hy]`, so `{(L_i)}_i=(L, \langle h \rangle, 0)`.
4. Solvability: note that `L` is an extension of `\ad L` by the center. Nilpotency: `\ad` is a hom, so termination of `{(\ad L)}_i=\ad L_i` implies `L_i\leq Z(L)` eventually.
5. For `\mathfrak h` with `[xy]=x`, the derived series is `(L, \langle x \rangle, 0)`, whereas the LCS stagnates at `\langle x \rangle`. For the other one with `[xy]=z,[xz]=y,[yz]=0`, evidently, every ideal must contain `I := \langle y, z \rangle`. We proceed to find `(L, I, 0)` as the DS and `(L, I, I, \ldots)` as the LCS.
6. `[L(A+B)]\leq [LA]+[LB]`, so `{(I+J)}_i` is bounded by `{(I)}_i+{(J)}_i`. For `\mathfrak h` this is `\langle x \rangle`, for the other one its center `\langle y, z \rangle`.
7. The lie algebra induced by `k\mapsto \alpha_k\colon (l+K\mapsto [kl]+K)` consists of nilpotent endomorphisms, so Engel guarantees a nonzero `l+K` in every kernel. This translates to `l` normalizing `K`.
8. Take `K\leq L` maximal and `l` as above. Normalization turns `K+ \mathsf{F} \cdot l` into a subalgebra, necessarily `L`. Also, `N_L(K)\geq K+\mathsf{F}l=L`.
9. First, `z\in C_L(K)` (nonempty because we have `Z(L)\neq 0`) ensures `\delta \in \operatorname{Der}(L)`. If we had `\delta=\ad_y` with `y\in L_i`, then `[yK]=0` – so `y\in C_L(K)` – and `[yx]=z` – so `z\in L_{i-1}`. Therefore, taking `z` to be of minimal “depth” `i` prevents such a `y\in C_L(K)` from existing.
10. Projection onto an ideal is a hom, so `{(L/K)}_i = L_i / K`. Ergo, `L_i\leq K` eventually. There, we have `(\ad_x)^{l+i}(y) \in (\ad_x)^l(K)` for any `x,y\in L`, vanishing for large enough `l`. Hence `L` is `ad`-nilpotent and by Engel nilpotent.
---
pub_date: 2020-07-07
