title: A note about ring extensions of fields
---
author: Lukas Juhrich
---
body:

Let $R$ be a commutative ring containing a field $K$ such that $R$ viewed as a $K$-Vector space has finite dimension. We then get a pretty neat thing:

**Observation** Let $r\in R$. The left multiplication map $$
\lambda\_r: R \to R,\quad x\mapsto r\cdot x
$$ is $K$-linear.

This enables us to observe some properties of ring elements by the behavior of their according left multiplication map. Note that if $r\in K$, the map is diagonal in every $K$-linear Basis of $R$.

**Lemma** $r$ is invertible if and only if it is not a zero divisor.

*Proof* If $r$ is not a zero divisor, $\lambda\_r$ must have trivial kernel, i.e. it is injective. By finite dimensionality, it is also surjective, hence $1\in \mathrm{Im} \lambda\_r$. Therefore, we can find an $s\in R$ such that $1=\lambda\_r (s) = rs$.
On the other hand, if $r$ is invertible, it cannot be a zero divisor, since for every $s\in r$, $0=rs$ implies $0 = r^{-1}rs = s$.<div class="qed"></div>
---
twitter_handle: xQuber
---
pub_date: 2019-05-02
